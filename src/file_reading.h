// header file for functions that read file
#ifndef FILE_READING_H
#define FILE_READING_H

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <chrono>
#include <memory>

#include "reading.h"
#include "analysis.h"

using namespace std;

typedef shared_ptr<reading> ptr;// smart (unique) pointer to base class objects
typedef map<int, vector<ptr>> DATA_MAP;// the main data container

extern const int HEADER_SPACING;
extern const int COLUMNS_IN_DAY;

void read_file(ifstream& FILE, DATA_MAP& m1, DATA_MAP& m2);
bool check_columns(int c);
void check_days(int days);
string convert_time(double t);
void write_file(double upper_limit, double lower_limit, string filename, const DATA_MAP& bg, const DATA_MAP& bolus);

#endif 