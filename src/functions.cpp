#include "functions.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <chrono>
#include <memory>

using namespace std;

typedef shared_ptr<reading> ptr;// smart (shared) pointer to base class objects
typedef map<int, vector<ptr>> DATA_MAP;// the main data container

void introduction() {// Just some ASCII art for fun....
	cout <<"          -------------------------------------------" << endl;
    cout <<"          -------------------------------------------" << endl;
    cout <<"          -------------------------------------------" << endl;
    cout <<"          ------------- W E L C O M E ---------------" << endl;
    cout <<"          -------------------------------------------" << endl;
	cout <<"          ----------------- T O ---------------------" << endl;
    cout <<"          -------------------------------------------" << endl;
	cout <<"          ----------- D I A N A L Y S E -------------" << endl;
    cout <<"          -------------------------------------------" << endl;
    cout <<"          -------------------------------------------" << endl;
    cout <<"          -------------------------------------------" << endl << endl;
}

void main_menu() {//default menu
	cout << "\nWhat would you like to do? Options are: " << endl;
	cout << "[a] Calculate averages of BG readings.\n" << endl;
	cout << "[b] Calculate total bolus doses.\n" << endl;
	cout << "[c] Print summaries to screen.\n" << endl;
	cout << "[x] Exit program and print summary file.\n" << endl;
}

bool check_key(int date, const DATA_MAP& m) {// checks if a key "date" is in the map "m". Return true if it is
	for (auto it = m.begin(); it != m.end(); it++) {
		if (date == it->first) return true;
	}
	return false;
}

void bg_menu() {// print bg menu
	cout << string(50, '\n');
	cout << "You have chosen to calculate averages & standard deviations of BG readings.\nDo you want to: " << endl;
	cout << "[a] Calculate the avg. & std. dev. of a certain day?\n" << endl;
	cout << "[b] Calculate the monthly BG avg. & std. dev. ?\n" << endl;
	cout << "[x] Exit to main menu?\n" << endl;
}

void bolus_menu() {// print bolus menu
	cout << string(50, '\n');
	cout << "You have chosen to calculate total bolus doses.\nDo you want to: " << endl;
	cout << "[a] Calculate the total bolus amount of a certain day?\n" << endl;
	cout << "[b] Calculate the total monthly bolus amount?\n" << endl;
	cout << "[x] Exit to main menu?\n" << endl;
}

void printing_menu() {// print the file menu
	cout << string(50, '\n');
	cout << "You have chosen to view/print BG readings or boluses.\nDo you want to: " << endl;
	cout << "[a] Print BG readings from a certain day to the screen?\n" << endl;
	cout << "[b] Print boluses from a certain day to the screen?\n" << endl;
	cout << "[c] Print a summary to the screen?\n" << endl;
	cout << "[x] Exit to main menu?\n" << endl;
}

void respond_main(char C, bool& r, const DATA_MAP& BG, const DATA_MAP& BOLUS) {// deal with responses to the main menu
	if (C == 'a') {
		bg_menu();
		char second_response;
		cin >> second_response;
			while (second_response != 'a' && second_response != 'b' && second_response != 'x' || cin.peek() != '\n') {// checks for invalid input
				cout << "Sorry I didn't get that. Please try again: " << endl;
				cin.clear(); cin.ignore(); cin >> second_response;
			}
			respond_bg_menu(second_response, BG);
		}
		else if (C == 'b') {
			bolus_menu();
			char second_response;
			cin >> second_response;
			while (second_response != 'a' && second_response != 'b' && second_response != 'x' || cin.peek() != '\n') {// checks for invalid input
				cout << "Sorry I didn't get that. Please try again: " << endl;
				cin.clear(); cin.ignore(); cin >> second_response;
			}
			respond_bolus_menu(second_response, BOLUS);
		}
		else if (C == 'c') {
			printing_menu();
			char second_response;
			cin >> second_response;
			while (second_response != 'a' && second_response != 'b' && second_response != 'c' && second_response != 'x' || cin.peek() != '\n') {
				cout << "Sorry I didn't get that. Please try again: " << endl;
				cin.clear(); cin.ignore(); cin >> second_response;
			}
			respond_printing_menu(second_response, BG, BOLUS);
		}
		else {// x input
			r = false;
		}
}

void respond_bg_menu(char C, const DATA_MAP& BG) {// deal with responses to the bg menu
	if (C == 'a') {
		cout << string(50, '\n');
		cout << "You have chosen to find the average BG of a certain day.\nPlease enter the date: " << endl;
		int date;
		cin >> date;
		while (cin.fail() || !check_key(date, BG)) {// check if date is in the BG map
			if (cin.fail()) cerr << "Error. Please enter a valid date: " << endl;
			else if (!check_key(date, BG)) cerr << "Error. Please enter a date which has data!" << endl;
			cin.clear(); cin.ignore(); cin >> date;
		}
		try {
			cout << "The BG average for day " << date << " is: " << daily::get_bg_average(BG.at(date)) << " mmol/l" << endl;// print avg
		}
		catch(...) {// if no bg data
			cerr << "Error. No BG data recorded for this day!" << endl;
		}
		try {
			cout << "The standard deviation for day " << date << " is: " << daily::get_bg_sd(BG.at(date)) << " mmol/l" << endl;// print std dev
		}
		catch(...) {// if only 1 bg data member
			cout << "The standard deviation for day " << date << " is: 0 mmol/l" << endl;// print std dev
		}
		system("pause");
		cout << string(50, '\n');
	}
	else if (C == 'b') {
		cout << string(50, '\n');
		try {
			cout << "The monthly BG average is: " << monthly::get_bg_average(BG) << " mmol/l" << endl;// monthly avg
		}
		catch(...) {// no bg data present
			cerr << "Error. No BG data in this month!" << endl;
		}
		try {
			cout << "The monthly BG standard deviation is: " << monthly::get_bg_sd(BG) << " mmol/l" << endl;// monthly sd
		}
		catch(...) {// only 1 bg data member
			cout << "The monthly standard deviation is: 0 mmol/l" << endl;
		}
		system("pause");
		cout << string(50, '\n');
	}
	else if (C == 'x') {
		cout << string(50, '\n');
		cout << "Exiting to main menu" << endl;
	}
}

void respond_bolus_menu(char C, const DATA_MAP& BOLUS) {// deal with responses to bolus menu
	if (C == 'a') {
		cout << string(50, '\n');
		cout << "You have chosen to find the total bolus of a certain day.\nPlease enter the date: " << endl;
		int date;
		cin >> date;
		while (cin.fail() || !check_key(date, BOLUS)) {// check for date that appears in the BOLUS map
			if (cin.fail()) cerr << "Error. Please enter a valid date: " << endl;
			else if (!check_key(date, BOLUS)) cerr << "Error. Please a date which has data!" << endl;
			cin.clear(); cin.ignore(); cin >> date;
		}
		cout << "The bolus total for day " << date << " is: " << daily::get_bolus_total(BOLUS.at(date)) << " units" << endl;
		system("pause");
		cout << string(50, '\n');
	}
	else if (C == 'b') {
		cout << string(50, '\n');
		cout << "The total monthly bolus dose was: " << monthly::get_bolus_total(BOLUS) << " units" << endl;
		system("pause");
		cout << string(50, '\n');
	}
	else if (C == 'x') {
		cout << string(50, '\n');
		cout << "Exiting to main menu" << endl;
	}
}

void respond_printing_menu(char C, const DATA_MAP& BG, const DATA_MAP& BOLUS) {// deal with responses to file printing menu
	if (C == 'a') {
		cout << string(50, '\n');
		cout << "You have chosen to print the BG readings of a certain day.\nPlease enter the date: " << endl;
		int date;
		cin >> date;
		while (cin.fail() || !check_key(date, BG)) {// check for valid date that appears in the BG map
			cerr << "Error. Please enter a valid date: " << endl;
			cin.clear(); cin.ignore(); cin >> date;
		}
		daily::print_bg(BG.at(date));
	}
	else if (C == 'b') {
		cout << string(50, '\n');
		cout << "You have chosen to print the boluses for a certain day.\nPlease enter the date: " << endl;
		int date;
		cin >> date;
		while (cin.fail() || !check_key(date, BOLUS)) {// check for valid date that appears in the BG map
			cerr << "Error. Please enter a valid date: " << endl;
			cin.clear(); cin.ignore(); cin >> date;
		}
		daily::print_bolus(BOLUS.at(date));
	}
	else if (C == 'c') {
		cout << string(50, '\n');
		cout << "Summary: " << endl << endl;
		try {
			cout << "Monthly BG average of " << BG::number_of_entries() << " entries : " << monthly::get_bg_average(BG) << " mmol/l" << endl;
		}
		catch(...) {// no BG data
			cerr << "Error. No BG data in the file!" << endl;
		}
		try {
			cout << "Monthly BG standard deviation: " << monthly::get_bg_sd(BG) << " mmol/l" << endl;
		}
		catch(...) {
			cout << "Monthly BG standard deviation: 0 mmol/l" << endl;
		}
		cout << "Total bolus dose: " << monthly::get_bolus_total(BOLUS) << " units" << endl;
		cout << "There were " << BOLUS::number_of_entries() << " boluses, of which " << BOLUS::number_of_correction();
		cout << " were corrections and " << BOLUS::number_of_food() << " were food boluses." << endl;
		system("pause");
		cout << string(50, '\n');
	}
	else if (C == 'x') {
		cout << string(50, '\n');
		cout << "Exiting to main menu" << endl;
	}
}