#include "file_reading.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <chrono>
#include <memory>

using namespace std;

typedef shared_ptr<reading> ptr;// smart (shared) pointer to base class objects
typedef map<int, vector<ptr>> DATA_MAP;// the main data container

void read_file(ifstream& FILE, DATA_MAP& m1, DATA_MAP& m2) {
	string line;// temporarily store line information
	int number_of_lines(0);// Keep track of line number
	int number_of_columns(0);// Counts the number of columns
	double time_of_day(0);// Keep track of the time of entry using key described at start of the main file
	while(getline(FILE,line)) {
		int data_number(0);// Keep track of the number of data points
		if (number_of_lines == 1) {// use this row to count the number of columns
			istringstream s(line);
			string entry;
			while(getline(s,entry,',')) {
				number_of_columns++;
			}// The number of columns should be a multiple of 5 because there are 5 headings per day....
			if(!check_columns(number_of_columns % COLUMNS_IN_DAY)) throw 1;//...therefore make sure number of columns is OK before trying to read file
		}
		if (number_of_lines > HEADER_SPACING) {// skip first two lines due to headers
			istringstream s_line(line);
			string entry;
			while(getline(s_line, entry,',')) {
				data_number++;
				if (data_number % COLUMNS_IN_DAY == 1) {// bg reading
					if (!entry.empty()) {// if bg data not empty
						// date determined by position of the data number. Reason for "+1" is because of integer division
						const int bg_date(data_number / COLUMNS_IN_DAY + 1);
						istringstream s_entry(entry);
						double double_entry;
						s_entry >> double_entry;
						if (!s_entry.fail()) {// if the conversion of string to double doesn't fail
							try {
								ptr temp_ptr(new BG(convert_time(time_of_day), double_entry));
								m1[bg_date].push_back(temp_ptr);
							}
							catch (bad_alloc memory_failure) {// exit in case of bad memory allocation
								cerr << "Error. Memory allocation failure." << endl;
								exit(1);
							}
							catch(...) {// Other exceptions - only bad time conversion
								cerr << "Error. Tried to convert invalid time" << endl;
								cerr << "There are possibly too many rows in the data file. Exiting now" << endl;
								exit(1);
							}
						}
						else cerr << "Warning. Bad datatype in BG column of day " << bg_date << " . Ignoring it." << endl;
					}
				}
				if (data_number % COLUMNS_IN_DAY == 4) {// bolus reading
					if(!entry.empty()) {// if bolus data not empty
						// determine the date from the position of the data number. +1 is because of integer division
						const int bolus_date((data_number + 1) / COLUMNS_IN_DAY);
						istringstream s_entry(entry);
						double double_entry;
						s_entry >> double_entry;
						if (!s_entry.fail()) {// if the conversion of string to double doesn't fail
							if (s_line.peek() == 'F') {// food bolus
								try {
									ptr temp_ptr(new BOLUS(convert_time(time_of_day), double_entry, 'F'));
									m2[bolus_date].push_back(temp_ptr);
								}
								catch (bad_alloc memory_failure) {// exit in case of bad memory allocation
									cerr << "Error. Memory allocation failure." << endl;
									exit(1);
								}
								catch(...) {// Other exceptions - only bad time conversion
									cerr << "Error. Tried to convert invalid time." << endl;
									cerr << "There are possibly too many rows in the data file. Exiting now" << endl;
									exit(1);
								}
							}
							else if (s_line.peek() == 'C') {// correction bolus
								try {
									ptr temp_ptr(new BOLUS(convert_time(time_of_day), double_entry, 'C'));
									m2[bolus_date].push_back(temp_ptr);
								}
								catch (bad_alloc memory_failure) {// exit in case of bad memory allocation
									cerr << "Error. Memory allocation failure." << endl;
									exit(1);
								}
								catch(...) {// Other exceptions - only bad time conversion 
									cerr << "Error. Tried to convert invalid time" << endl;
									cerr << "There are possibly too many rows in the data file. Exiting now" << endl;
									exit(1);
								}
							}
							else {// if there is empty bolus type or incorrect bolus type
								cout << string(50, '\n');
								cout << "***Warning.***\nBolus with invalid type in data file in day " << bolus_date << "." << endl;
								cout << "Assuming this is a mistake and ignoring it" << endl;
								system("pause");
								cout << string(50, '\n');
							}
						}
						else cerr << "Warning. Bad datatype in bolus column of day " << bolus_date << " . Ignoring it." << endl;
					}
				}
			}
			number_of_lines++;
			time_of_day += 0.5;
		}
		else {// increment counter
			number_of_lines++;
		}
	}
	cout << "Checking the number of days ... " << endl << endl;
	system("pause");
	check_days(number_of_columns / COLUMNS_IN_DAY);// Check the number of days
}

bool check_columns(int c) {// Check the number of columns is OK
	if (c != 0) {// If total number of columns / number of headings in a day is not an integer,
		return false;//    then there is a rogue column in the file!
	}
	else return true;
}

void check_days(int days) {// Check the number of days in the file. Print warning if there are not 28 - 31 days
	if (days < 28) {
		cout << string(50, '\n');
		cout << "***Warning!***\n\nThere are less days in the file than days in a month." << endl << endl;
		cout << "Calculating information anyway" << endl;
		system("pause");
		cout << string(50, '\n');
	}
	if (days > 31) {
		cout << string(50, '\n');
		cout << "***Warning!*** There are more days in the file than days in a month." << endl;
		cout << "Calculating information anyway" << endl;
		system("pause");
		cout << string(50, '\n');
	}
}

string convert_time(double t) {// Convert the time from say 1.5 to 1:30 AM. They conversion for this is described in the main.cpp file
	if (t < 0. || t > 24. ) {// Invalid range
		throw 1;
	}
	if (fmod(t / 0.5, 1) != 0) {// Something like 3.4 input
		throw 2;
	}
	stringstream output;
	const string half(":30");
	const string zero(":00");
	const string am(" AM");
	const string pm(" PM");
	if (fmod(t, 1) != 0) {// there is a half hour in the time
		t -= 0.5;// remove the 0.5 from the variable
		if (t > 12) {// Greater than 12:00 PM
			t -= 12;// So we can print in 12 hour format
			output << t << half << pm;
		}
		else if (t == 12) {
			output << t << half << pm;
		}
		else {// Less than or equal to 12:00 PM
			output << t << half << am;
		}
	}
	else {// no half hour in the time
		if (t > 12) {// Greater than 12:00 PM
			t -= 12;
			output << t << zero << pm;
		}
		else if (t == 12) {
			output << t << zero << pm;
		}
		else {// Less than or equal to 12:00 PM
			output << t << zero << am;
		}
	}
	return output.str();// convert string stream to string
}

void write_file(double upper_limit, double lower_limit, string filename, const DATA_MAP& bg, const DATA_MAP& bolus) {// write summary file
	const string output_filename("summary_data.txt");
	cout << "Writing summary of " << filename << " to file " << output_filename << endl;
	ofstream OUTPUT(output_filename.c_str());// output file stream
	if (OUTPUT.good()) {// check if file is open
		
		chrono::system_clock::time_point today = chrono::system_clock::now();// get current system time
		time_t current_time(chrono::system_clock::to_time_t(today));
		
		OUTPUT << setprecision(1) << fixed;// fix output to 2 dp
		OUTPUT << "Summary for file: " << filename << "." << endl;
		OUTPUT << "This summary was created " << ctime(&current_time) << endl;// date & timestamp
		if (BOLUS::number_of_entries() > 0) {// print bolus summary if there are boluses!
			OUTPUT << "--------------------------------------------------------------------------------------------" << endl << endl;
			OUTPUT << "Bolus summaries" << endl << endl;
			OUTPUT << "Total bolus dose: " << monthly::get_bolus_total(bolus) << " units" << endl;
			OUTPUT << "There were " << BOLUS::number_of_entries() << " boluses." << endl;
			OUTPUT << "Number of correction boluses: " << BOLUS::number_of_correction() << endl;
			OUTPUT << "Number of food boluses: " << BOLUS::number_of_food() << endl;
			
			double avg_daily_bolus(0);
			for (auto it_map = bolus.begin(); it_map != bolus.end(); ++it_map) {// calculate average total daily bolus dose
				avg_daily_bolus += daily::get_bolus_total(it_map->second);
			}
			avg_daily_bolus /= bolus.size();
			OUTPUT << "Average daily bolus dose: " << avg_daily_bolus << " units" << endl;
		}

		if (BG::number_of_entries() > 0) {// print bg summary if there are bg entries!
			OUTPUT << "--------------------------------------------------------------------------------------------" << endl << endl;
			OUTPUT << "BG summaries" << endl << endl;
			int number_low(0);
			int number_high(0);
			for (auto it_map = bg.begin(); it_map != bg.end(); ++it_map) {// count bg readings outside high and low limits
				try {
					number_low += count_if(it_map->second.begin(), it_map->second.end(),
						[lower_limit] (ptr x) { // lambda function to sum the total number of readings below the low limit
							return (x->get_bg() < lower_limit); 
						}
					);
					
					number_high += count_if(it_map->second.begin(), it_map->second.end(),
						[upper_limit] (ptr x) {// lambda function to sum the total number of readings above the high limit
							return (x->get_bg() > upper_limit);
						}
					);
				}
				catch(...) {// shouldn't be needed but just incase
					cerr << "Error. Tried to get bg data of a bolus" << endl;
				}
			}
			OUTPUT << "There were " << BG::number_of_entries() << " bg entries." << endl;
			OUTPUT << "Percentage above high limit " << upper_limit << ": " << (double) number_high * 100 / BG::number_of_entries() << endl;
			OUTPUT << "Percentage above low limit " << lower_limit << ": " << (double) number_low * 100 / BG::number_of_entries() << endl;
			OUTPUT << "Percentage in range: " << 100 - 100 * (double) (number_high + number_low) / BG::number_of_entries() << endl;
			OUTPUT << "Monthly BG average: " << monthly::get_bg_average(bg) << " +/- " << monthly::get_bg_sd(bg) << " mmol/l" << endl;
		}
	}
	else {
		cerr << "Error writing to file. Exiting now." << endl;// program exits after this [main.cpp]
	}
	OUTPUT.close();
}