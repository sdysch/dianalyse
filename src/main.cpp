/*******************************************************************************************************************************************************

Note! *Most* of the explanation for the operation of this program is in my report. Please read that first!

Typical low and high bg limits that can be entered by the user are:
																	low: 4.3
																	high: 8.8

*******************************************************************************************************************************************************/

// header files - some of these include each other but multiple definitions prevented by header guards
#include "reading.h"// header file for classes
#include "analysis.h"// header file for analysis functions
#include "file_reading.h"// header file for file reading/writing functions
#include "functions.h"// header file for main program functions

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <chrono>
#include <memory>

using namespace std;

typedef shared_ptr<reading> ptr;// smart (shared) pointer to base class objects
typedef map<int, vector<ptr>> DATA_MAP;// the main data container

int BG::bg_count(0);// initialize static members
int BOLUS::bolus_count(0);
int BOLUS::food_count(0);
int BOLUS::correction_count(0);

const int HEADER_SPACING(1);// the number of rows (1) that are occupied with headers
const int COLUMNS_IN_DAY(5);// the number of columns in a day (currently bg, basal, ketone, bolus and type)

DATA_MAP bg_data;// store bg data. Key of the map is date
DATA_MAP bolus_data;// store bolus data. Key of the map is date

int main() {
	cout << setprecision(1) << fixed;// fix output of doubles to 2 dp
	cout << "The use of this program *should* be fairly intuitive,\nbut please read the report & proposal which explains the\nfunctionality of this program in more detail. " << endl;
	system("PAUSE");
	cout << string(50, '\n');
	introduction();// print the introduction menu
	
	/*********************************************file reading*********************************************/
	
	ifstream FILE;
	string filename;
	cout << "\nPlease enter the name of the file (with extension) you wish to analyse\nor type 'x' to exit: " << endl;
	while(true) {// Check if file is open & prompt retry if not
		getline(cin, filename);
		if (filename == "x") {// exit program
			cout << "Thank you for using Dianalyse! Live long and bolus!" << endl;
			return 0;
		}
		FILE.open(filename.c_str());
		if (FILE.good()) {// if file has been opened OK then exit infinite loop
			cout << "File opened successfully!" << endl;
			break;
		}
		cerr << "Error opening file of name " << filename;
		cout << ". Please try again: ";
	}
	try {
		read_file(FILE, bg_data, bolus_data);// calls function to read data from the file
	}
	catch (...) {// Check for number of columns != a multiple of number of headings. If this is the case, exit program
		cerr << "Error. There is an incorrect number of columns in the file." << endl;
		cerr << "There should be an integer multiple of " << COLUMNS_IN_DAY << " columns in total." << endl;
		cerr << "Please check the data file." << endl;
		cout << "Exiting now." << endl;
		return(1);
	}
	FILE.close();
	cout << "File read successfully!" << endl;
	if (BG::number_of_entries() == 0 && BOLUS::number_of_entries() == 0) {
		cerr << "Error. There is no data in the file to analyse!" << endl;
		return(1);
	}
	cout << "Read in " << BG::number_of_entries() << " BG entries and " << BOLUS::number_of_entries() << " bolus entries" << endl;
	cout << "Please enter your lower BG limit in mmol/l (typical value is 4.3) : " << endl;
	double lower_limit;
	cin >> lower_limit;
	while (cin.fail() || lower_limit < 0) {
		if (cin.fail()) {
			cerr << "Invalid input. Please enter a number: " << endl;
		}
		else if (lower_limit < 0) {
			cerr << "lower limit must be greater than 0!" << endl;
		}
		cin.clear(); cin.ignore(); cin >> lower_limit;
	}
	cout << "Please enter your upper BG limit in mmol/l (typical value is 8.8): " << endl;
	double upper_limit;
	cin >> upper_limit;
	while (cin.fail() || upper_limit < lower_limit) {
		if (cin.fail()) {
			cerr << "Invalid input. Please enter a number: " << endl;
		}
		else if (upper_limit < lower_limit) {
			cerr << "upper limit must be greater than lower limit!" << endl;
		}
		cin.clear(); cin.ignore(); cin >> upper_limit;
	}
	/***********************************************main menu/functions loop for analysis******************************/
	
	bool repeat_program(true);// bool to see if user wants to run program again
	do {
		main_menu();
		char main_response;
		cin >> main_response;
		while (main_response != 'a' && main_response != 'b' && main_response != 'c' && main_response != 'x') {// invalid choice entered
			cout << "Sorry I didn't get that. Please try again: " << endl;
			cin.clear(); cin.ignore(); cin >> main_response;
		}
		respond_main(main_response, repeat_program, bg_data, bolus_data);// next menu loop
	} while (repeat_program);
	cout << string(50, '\n');
	write_file(upper_limit, lower_limit, filename, bg_data, bolus_data);
	cout << "Thank you for using Dianalyse! Live long and bolus!" << endl;
	return 0;
}
