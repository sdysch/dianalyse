#include "reading.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <chrono>
#include <memory>

using namespace std;

BG::BG() : time("empty"), bg(0) {};// default constructor

BG::BG(string time_, double bg_) : time(time_), bg(bg_) {bg_count++;};// parameterized constructor

void BG::print() const {// print information
	cout << "Time: " << time << "\tBG: " << bg << " mmol/l" << endl;
}

double BG::get_bg() const {
	return bg;
}

double BG::get_bolus() const {
	throw 2;// no bolus data for bg
}

char BG::get_type() const {
	throw 2;// no type for bg
}

string BG::get_time() const {
	return time;
}

int BG::number_of_entries() {
	return bg_count;
}

int BG::number_of_correction() {
	cerr << "There are no correction boluses in the BG class!" << endl;
	return 0;
}

int BG::number_of_food() {
	cerr << "There are no food boluses in the BG class!" << endl;
	return 0;
}

/************************functions for BOLUS class**********************************/

BOLUS::BOLUS() : time("empty"), bolus(0), type('U') {};// default constructor (U for unsigned)

BOLUS::BOLUS(string time_, double bolus_, char type_) : time(time_), bolus(bolus_), type(type_) {
	bolus_count++;
	if (type_ == 'C') correction_count++;
	else if (type_ == 'F') food_count++;
}// parameterized constructor

void BOLUS::print() const {// print information
	cout << "Time: " << time << "\tBolus: " << bolus << " " << type << endl;
}

double BOLUS::get_bg() const {
	throw 2;// no bg data for a bolus
}

double BOLUS::get_bolus() const {
	return bolus;
}

char BOLUS::get_type() const {
	return type;
}

string BOLUS::get_time() const {
	return time;
}

int BOLUS::number_of_entries() {
	return bolus_count;
}

int BOLUS::number_of_correction() {
	return correction_count;
}

int BOLUS::number_of_food() {
	return food_count;
}