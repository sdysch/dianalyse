#include "analysis.h"

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <chrono>
#include <memory>

using namespace std;

typedef shared_ptr<reading> ptr;// smart (shared) pointer to base class objects
typedef map<int, vector<ptr>> DATA_MAP;// the main data container

namespace daily {
	double get_bg_average(const vector<ptr>& v) {// get the average of all the bg readings in one day
		int results(0);
		double total(0);
		for (auto it = v.begin(); it < v.end(); ++it) {
			try {
				total += (*it)->get_bg();
				results++;
			}
			catch(...) {// This shouldn't be needed, but put just incase to avoid the programming crashing.
				cerr << "Error. Tried to access bolus information instead of BG information" << endl;
			}
		}
		if (results == 0) throw 1;// can't have average if no data!
		else return total / results;
	}

	double get_bg_sd(const vector<ptr>& v) {// std dev of BG readings in one day
		int results(0);
		double total(0);
		const double mean(get_bg_average(v));
		for (auto it = v.begin(); it < v.end(); ++it) {
			try {
				total += pow((*it)->get_bg() - mean, 2);
				results++;
			}
			catch(...) {// This shouldn't be needed, but put just incase to avoid the programming crashing.
				cerr << "Error. Tried to access bolus information instead of BG information" << endl;
			}
		}
		if (results - 1 == 0) throw 1;
		else return sqrt(total / (results - 1));
	}

	void print_bg(const vector<ptr>& v) {// print a days' bg readings
		for (auto it = v.begin(); it < v.end(); ++it) {
			(*it)->print();
		}
	}

	void print_bolus(const vector<ptr>& v) {// print a days' boluses
		for (auto it = v.begin(); it < v.end(); ++it) {
			(*it)->print();
		}
	}

	double get_bolus_total(const vector<ptr>& v) {// total bolus amount for a day
		double total(0);
		for (auto it = v.begin(); it < v.end(); ++it) {
			try {
				total += (*it)->get_bolus();
			}
			catch(...) {// This shouldn't be needed, but put just incase to avoid the programming crashing.
				cerr << "Error. Tried to access bolus information instead of BG information" << endl;
			}
		}
		return total;
	}
};

namespace monthly {
	double get_bg_average(const DATA_MAP& m) {// get the average of all bg data that has been read in
		int results(0);
		double total(0);
		for (auto it_map = m.begin(); it_map != m.end(); ++it_map) {
			for (auto it_vec = it_map->second.begin(); it_vec != it_map->second.end(); ++it_vec) {
				try { 
					total += (*it_vec)->get_bg();
					results++;
				}
				catch(...) {// This shouldn't be needed, but put just incase to avoid the programming crashing.
					cerr << "Error. Tried to access bolus information instead of BG information" << endl;
				}
			}
		}
		if (results == 0) throw 1;// can't have average if no data!
		else return total / results;
	}

	double get_bg_sd(const DATA_MAP& m) {// monthly bg standard deviation
		int results(0);
		double total(0);
		const double mean(get_bg_average(m));
		for (auto it_map = m.begin(); it_map != m.end(); ++it_map) {
			for (auto it_vec = it_map->second.begin(); it_vec != it_map->second.end(); ++it_vec) {
				try {
					total += pow((*it_vec)->get_bg() - mean, 2);
					results++;
				}
				catch(...) {// This shouldn't be needed, but put just incase to avoid the programming crashing.
					cerr << "Error. Tried to access bolus information instead of BG information" << endl;
				}
			}
		}
		if (results - 1 == 0) throw 1;
		else return sqrt(total / (results - 1));
	}

	double get_bolus_total(const DATA_MAP& m) {// total bolus amount for the entire month
		double total(0);
		for (auto it_map = m.begin(); it_map != m.end(); ++it_map) {
			for (auto it_vec = it_map->second.begin(); it_vec != it_map->second.end(); ++it_vec) {
				try {
					total += (*it_vec)->get_bolus();
				}
				catch(...) {// This shouldn't be needed, but put just incase to avoid the programming crashing.
					cerr << "Error. Tried to access bolus information instead of BG information" << endl;
				}
			}
		}
		return total;
	}
};