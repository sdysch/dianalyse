// header file for base class & subsequent derived classes
#ifndef READING_H
#define READING_H

#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <map>
#include <cmath>
#include <cstdlib>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <chrono>
#include <memory>

using namespace std;

// Abstract base class for a reading
class reading {
public:
	virtual ~reading() {}
	virtual void print() const = 0;
	virtual double get_bg() const = 0;
	virtual double get_bolus() const = 0;
	virtual char get_type() const = 0;
	virtual string get_time() const = 0;
	
	static int number_of_entries();// prototype needed for polymorphism but no implementation of function
	static int number_of_food();
	static int number_of_correction();
};

// Derived bg class
class BG : public reading {
private:
	string time;
	double bg;
	static int bg_count;
public:
	BG();// default constructor
	BG(string time_, double bg_);// parameterized constructor
	~BG() {};// destructor
	void print() const;// print information
	double get_bg() const;// getter for bg
	double get_bolus() const;// getter for bolus - prints error
	char get_type() const;// getter for bolus type - prints error
	string get_time() const;// getter for time
	
	static int number_of_entries();// static functions to return static data!
	static int number_of_food();// prints error and returns 0
	static int number_of_correction();// prints error and returns 0
};

// Derived bolus class
class BOLUS : public reading {
private:
	string time;
	double bolus;
	char type;
	static int bolus_count;
	static int food_count;
	static int correction_count;
public:
	BOLUS();// default constructor
	BOLUS(string time_, double bolus_, char type_);// parameterized constructor
	~BOLUS() {};// destructor
	void print() const;// print information
	double get_bg() const;// getter for bg
	double get_bolus() const;// getter for bolus
	char get_type() const;// getter for bolus type
	string get_time() const;// getter for time
	
	static int number_of_entries();// static functions to return static data!
	static int number_of_food();
	static int number_of_correction();
};

#endif