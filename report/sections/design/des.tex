The program is separated across multiple header and source files:
\begin{itemize}
\item \texttt{reading.h} and \texttt{reading.cpp} contain the class declarations, implementations and derived classes.
\item \texttt{file\_reading.h} and \texttt{file\_reading.cpp} contain functions for reading the file data and writing the summary file.
\item \texttt{analysis.h} and \texttt{analysis.cpp} contain functions for performing an analysis of the stored data.
\item \texttt{functions.h} and \texttt{functions.cpp} contain functions for controlling the flow of the program via the main user interface.
\item \texttt{main.cpp} contains the main program loop.
\end{itemize}
\subsection{Classes}
The code contains an abstract base class, \texttt{reading}, and two derived classes from it, \texttt{BG} and \texttt{BOLUS}. The \texttt{BG} class contains private variables for: the BG reading of type double, a string for the time the reading was taken and a static integer to count the total number of \texttt{BG} objects created. Each time the \texttt{BG} constructor is called, the static integer count is increased by 1.

The \texttt{BOLUS} class contains private variables for: the bolus reading of type double, a string for the time the bolus was given, a character for the type of bolus (food or correction) and static integers for the total number of \texttt{BOLUS} objects created, as well as for the total number of food and correction boluses. Each time the \texttt{BOLUS} constructor is called, the static integer corresponding to the total number of instances of \texttt{BOLUS} is incremented by 1. Additionally, the appropriate static integer corresponding to either a food or correction bolus is incremented, depending on the type of bolus being constructed.

Finally, each class contains member functions to return the private data. Because polymorphism is used, there must be an instance of every function in the abstract base class in each derived class. For example, a function \texttt{get\_bg} appears in the \texttt{BOLUS} class, which prints a warning and throws an exception because a BG reading cannot be a bolus reading. This follows for all member functions which occur in classes where their use does not make sense. The program is prevented from runtime errors by utilising appropriate \texttt{catch} blocks where necessary.

Two container classes from the STL are used to store the data; \texttt{map} and \texttt{vector}. A single day's data is stored in a \texttt{vector} of \texttt{shared\_ptr}s to the abstract base class and each of these vectors is contained within a \texttt{map}; the key of the \texttt{map} is an integer which corresponds to the date of each day of results. For convenience, the smart pointers and data containers were renamed using the following lines of code:
\begin{lstlisting}
typedef shared_ptr <reading> ptr;
typedef map <int, vector<ptr>> DATA_MAP;
\end{lstlisting}
There are two \texttt{DATA\_MAP}s used in the code; one which stores BG data and another which stores bolus data. These are called \texttt{bg\_data} and \texttt{bolus\_data}, respectively.
\subsection{Program Flow}
Upon starting the program, the user is initially asked to enter the file which they would like to analyse. The code then reads the input file (this is described in more detail in the next section). Afterwards, the user is prompted to enter their \emph{low} and \emph{high} BG limits for a target range. Finally, the main flow of the program is controlled inside a \texttt{do-while} loop by a tree of menu options, schematically shown in Fig.~\ref{fig:tree}.

%%%%%%%%%%%%%%% FLOW CHART%%%%%%%%%%%%%%%%%%%%%%%%%%%
\tikzstyle{startstop} = [rectangle, rounded corners, minimum width=3cm, minimum height=1cm,text centered, draw=black]
\tikzstyle{process} = [rectangle, minimum width=1cm, minimum height=1cm, text centered, draw=black, text width=4cm]
\tikzstyle{arrow} = [thick,->,>=stealth]

\begin{figure}[h!]
\centering
\begin{tikzpicture}[node distance=2cm, scale=0.65, every node/.style={scale=0.65}]
\node (start) [startstop] {Main menu};


\node (main1) [process, right of=start, xshift=2cm] {BG options};
\node (main2) [process, below of=main1, yshift=0.7cm] {Bolus options};
\node (main3) [process, below of=main2, yshift=0.7cm] {Printing options};
\node (main4) [process, below of=main3, yshift=0.7cm] {Exit program};


\node(bg1) [process, right of=main1, xshift=3cm] {BG analysis functions};
\node(bg2) [process, below of=bg1, yshift=0.7cm] {Exit to main menu?};

\node(bolus1) [process, below of=bg2, yshift=0.7cm] {Bolus analysis functions};
\node(bolus2) [process, below of=bolus1, yshift=0.7cm] {Exit to main menu?};

\node(print1) [process, below of=bolus2, yshift=0.7cm] {Printing functions};
\node(print2) [process, below of=print1, yshift=0.7cm] {Exit to main menu?};


\node(final) [startstop, right of=bolus1, xshift=3cm, yshift=-0.35cm] {Go to main menu};


\draw [arrow] (start) -- (main1);
\draw [arrow] (start) |- (main2);
\draw [arrow] (start) |- (main3);
\draw [arrow] (start) |- (main4);

\draw [arrow] (main1) -- (bg1);
\draw [arrow] (main1) -- (bg2);

\draw [arrow] (main2) -- (bolus1);
\draw [arrow] (main2) -- (bolus2);

\draw [arrow] (main3) -- (print1);
\draw [arrow] (main3) -- (print2);

\draw [arrow] (bg1) -| (final);
\draw [arrow] (bg2) -| (final);
\draw [arrow] (bolus1) -- (final);
\draw [arrow] (bolus2) -- (final);
\draw [arrow] (print1) -| (final);
\draw [arrow] (print2) -| (final);
\end{tikzpicture}
\caption{The menu tree that forms the main program.}
\label{fig:tree}
\end{figure}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\noindent The available analysis options are selected by entering the appropriate character corresponding to the choice. At each stage, these options are:\\
\textbf{BG analysis functions}
\begin{itemize}
\item Print BG averages and standard deviations from a certain day.
\item Print the BG average and standard deviation for the whole file.
\end{itemize}
\textbf{Bolus analysis functions}
\begin{itemize}
\item Print total bolus amount for a certain day.
\item Print total bolus amount for the whole file
\end{itemize}
\textbf{Printing functions}
\begin{itemize}
\item Print BG readings from a certain day
\item Print boluses from a certain day
\item Print a short summary of the file to the screen 
\end{itemize}
\subsection{File Reading}
The program is designed to work with a specific format of the input \texttt{.csv} file. In the file, the data is stored in multiple groups of five columns, with two rows for headings. Each row represents a 30 minute time interval starting from 12:00 AM and ending at 11:30 PM, meaning there are $2+48=50$ rows in total. A preview of the file structure is shown in Table~\ref{tab:cols}.
\begin{table}[h!]
\centering
\tabcolsep=0.11cm
\begin{tabular}{|c|c|c|c|c|c|c|c|c|c|c|c|}
\hline
&&1&&&&&2&&&$\cdots$\\
\hline
BG&ket&basal&bolus&type&BG&ket&basal&bolus&type&$\cdots$\\
\hline
5.7&&0.9&&&&&0.9&4.5&F&$\cdots$\\
\hline
$\vdots$&$\vdots$&$\vdots$&$\vdots$&$\vdots$&$\vdots$&$\vdots$&$\vdots$&$\vdots$&$\vdots$&$\cdots$\\
\hline
\end{tabular}
\caption{A preview of a possible data file. The first row contains headings for the date of the month, and the second contains headings for the type of entry in the column. The `ket' column is rarely used and can be ignored.}
\label{tab:cols}
\end{table}

The code reads in the file line by line, temporarily storing each line in a \texttt{stringstream}. The first row is skipped due to headers and the second row is used to calculate the total number of columns in the datafile. A check is made to ensure that the total number of columns is a multiple of the number of columns in a `group' (5) and an exception is thrown if this condition is not met;
\begin{lstlisting}
if(!check_columns(number_of_columns % 5)) throw 1;
\end{lstlisting}
If this exception is caught, an error message is printed prompting the user to check the datafile and the code terminates. However, if this exception is not thrown, then the code reads each comma-separated variable into a \texttt{stringstream}. The type of variable is determined by its position in the column group. For example, the position of a BG reading `\%' the column group number (5) will give 1. Provided the entry is not empty and writing the variable to a \texttt{double} does not fail, an appropriate \texttt{BG} or \texttt{BOLUS} object is constructed. The type of bolus is in the adjacent column to the bolus amount, so this can be determined by using the \texttt{peek()} function. This allows a check that a bolus always has a type associated with it, without storing two temporary variables; a warning is printed if this is not the case. If writing the data to a \texttt{double} fails, a warning message is printed and the datatype in question is ignored.

In order to determine the time that a reading was taken, a numeric conversion is used. Each time a new row is read in, a counter, which starts at 0, is incremented by 0.5. 0 represents 12:00 AM and each extra 0.5 represents a 30 minute interval. For example 17.5 represents 5:30 PM. A function, \texttt{convert\_time}, takes this time counter variable, \texttt{t}, as a parameter and returns a string corresponding to the time it represents. The implementation of this function is now summarised:
\begin{itemize}
\item Get \texttt{t} value
\item If \texttt{t} $<$ 0 or \texttt{t} $>$ 24 then throw exception (time out of range)
\item If \texttt{t} is not a multiple of 0.5 then throw an exception (invalid type for time)
\item If \texttt{t} is in the form $x$.5
\begin{itemize}
\item[*] If \texttt{t}$-$0.5 $>$ 12 then return a \texttt{string} \{(\texttt{t}$-$12.5) : 30 PM\}
\item[*] If \texttt{t}$-$0.5 $<$ 12 then return a \texttt{string} \{(\texttt{t}$-$0.5) : 30 AM\}
\item[*] If \texttt{t}$-0.5=$ 12 then return a \texttt{string} \{12 : 30 PM\}
\end{itemize}
\item else (\texttt{t} is not in the form $x$.5)
\begin{itemize}
\item[*] If \texttt{t} $>$ 12 then return a \texttt{string} \{\texttt{t} : 00 PM\}
\item[*] If \texttt{t} $<$ 12 then return a \texttt{string} \{\texttt{t} : 00 AM\}
\item[*] If \texttt{t} $=$ 12 then return a \texttt{string} \{12 : 00 PM\}
\end{itemize}
\end{itemize}
If there are more rows in the datafile than allowed by the program, then the \texttt{convert\_time} function will throw an exception relating to \texttt{t} $>$ 24. The exception is caught and the program terminates.

Finally, a check is made on the number of days in the datafile. If the number of days is not in between 28 and 31, then a warning message is printed notifying the user that the number of days does not correspond to an allowed number of days in a month. All of these functions are implemented in the \texttt{file\_reading.cpp} file.
\subsection{Analysis Functions}
The analysis operations available to the user come in two types: those which use data from a single day and those which use the entire file. Both perform similar operations and to avoid confusing function names, the daily and monthly analysis operations are grouped into namespaces \texttt{daily} and \texttt{monthly}, respectively.

The summary file that is automatically generated produces a summary of the data from the input file. A timestamp of when the summary file is written, as well as the total monthly bolus dose, average daily bolus dose, average BG readings, standard deviation of BG readings, and the number of each type of readings. When writing to the summary file, the program also uses two lambda functions and the \texttt{count\_if} function from the \texttt{algorithm} header to calculate the percentage of results that fall outside these ranges. For example, when iterating over the \texttt{DATA\_MAP bg} to calculate the number of low BG results, the following line of code is implemented at each iteration:
\begin{lstlisting}
number_low+=count_if(it_map->second.begin(), it_map->second.end(),
	[lower_limit] (ptr x) {
		return (x->get_bg() < lower_limit); 
	}
);
\end{lstlisting}
where \texttt{it\_map} is iterating over the \texttt{map}.
